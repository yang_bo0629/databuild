package com.bonc.databuild.service;

import com.bonc.databuild.form.BranchResultForm;

import java.util.List;

public interface BranchService {
    List<String> getRegionList(int type);

    List<String> getCityList(String region);

    List<String> getGriddingList(String region, String city, String gridding);

    List<String> getCity(String region, String city);

    List<BranchResultForm> queryRegionList(String regionCode);

    List<BranchResultForm> queryCityList00(String regionCode, String cityCode);

    List<BranchResultForm> queryCityList_(String regionCode, String cityCode);

    List<BranchResultForm> queryCityListNormal(String regionCode, String cityCode);

    List<BranchResultForm> queryGriddingList00(String parOrgId, String griddingCode);

    List<BranchResultForm> queryGriddingList_(String parOrgId, String griddingCode);

    List<BranchResultForm> queryGriddingListNormal(String parOrgId, String griddingCode);

    List<BranchResultForm> queryRegionListNew(String s);
}

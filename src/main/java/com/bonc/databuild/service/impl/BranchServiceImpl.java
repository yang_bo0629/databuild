package com.bonc.databuild.service.impl;

import com.bonc.databuild.dao.BranchMapper;
import com.bonc.databuild.form.BranchResultForm;
import com.bonc.databuild.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchServiceImpl implements BranchService {
    @Autowired
    private BranchMapper branchMapper;

    @Override
    public List<String> getRegionList(int type) {
        List<String> list = branchMapper.getRegionList(type);
        return list;
    }

    @Override
    public List<String> getCityList(String region) {
        List<String> list = branchMapper.getCityList(region);
        return list;
    }

    @Override
    public List<String> getGriddingList(String region, String city, String gridding) {
        List<String> list = branchMapper.getGriddingList(region,city,gridding);
        return list;
    }

    @Override
    public List<String> getCity(String region, String city) {
        List<String> list = branchMapper.getCity(region,city);
        return list;
    }

    @Override
    public List<BranchResultForm> queryRegionList(String regionCode) {
        return branchMapper.queryRegionList(regionCode);
    }

    @Override
    public List<BranchResultForm> queryCityList00(String regionCode, String cityCode) {
        return branchMapper.queryCityList00(regionCode,cityCode);
    }

    @Override
    public List<BranchResultForm> queryCityList_(String regionCode, String cityCode) {
        return branchMapper.queryCityList_(regionCode,cityCode);
    }

    @Override
    public List<BranchResultForm> queryCityListNormal(String regionCode, String cityCode) {
        return branchMapper.queryCityListNormal(regionCode,cityCode);
    }

    @Override
    public List<BranchResultForm> queryGriddingList00(String parOrgId, String griddingCode) {
        return branchMapper.queryGriddingList00(parOrgId,griddingCode);
    }

    @Override
    public List<BranchResultForm> queryGriddingList_(String parOrgId, String griddingCode) {
        return branchMapper.queryGriddingList_(parOrgId,griddingCode);
    }

    @Override
    public List<BranchResultForm> queryGriddingListNormal(String parOrgId, String griddingCode) {
        return branchMapper.queryGriddingListNormal(parOrgId,griddingCode);
    }

    @Override
    public List<BranchResultForm> queryRegionListNew(String s) {
        return branchMapper.queryRegionListNew(s);
    }


}

package com.bonc.databuild.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bonc.databuild.dao.QuotaInfoDao;
import com.bonc.databuild.entity.BusiCatagory;
import com.bonc.databuild.entity.QuotaInfoEntity;
import com.bonc.databuild.service.QuotaInfoService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("quotaInfoService")
public class QuotaInfoServiceImpl extends ServiceImpl<QuotaInfoDao, QuotaInfoEntity> implements QuotaInfoService {


    @Override
    public String getCycle(String quotaCode) {
        return this.getBaseMapper().getCycle(quotaCode);
    }

    @Override
    public String getQuotaType(String quotaCode) {
        return this.getBaseMapper().getQuotaType(quotaCode);
    }

    @Override
    public List<BusiCatagory> getBusiCatagoryList() {
        return this.getBaseMapper().getBusiCatagoryList();
    }
}

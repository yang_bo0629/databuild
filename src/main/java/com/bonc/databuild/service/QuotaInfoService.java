package com.bonc.databuild.service;

import com.bonc.databuild.entity.BusiCatagory;

import java.util.List;

public interface QuotaInfoService {
    String getCycle(String quotaCode);

    String getQuotaType(String item_code);

    List<BusiCatagory> getBusiCatagoryList();
}

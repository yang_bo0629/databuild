package com.bonc.databuild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class DatabuildApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatabuildApplication.class, args);
    }

}

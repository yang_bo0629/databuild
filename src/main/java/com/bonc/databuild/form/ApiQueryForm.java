package com.bonc.databuild.form;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiQueryForm implements Serializable {
    private Long apiId;
    private Long quotaId;
    private String quotaCode;
    private String region;
    private String city;
    private String gridding;
    private String statisticalDate;
    private String quotaCycle;
    private String quotaType;
}

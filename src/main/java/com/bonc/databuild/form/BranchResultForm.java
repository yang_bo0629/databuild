package com.bonc.databuild.form;

import lombok.Data;

/**
 * @Description  地市、区县、网格  三级联动参数表
 * @author yangbo
 * @Date 2021/9/26 15:15
 */
@Data
public class BranchResultForm {
    //编码
    private String code;
    //名称
    private String name;
    //父级编码
    private String pCode;

}

package com.bonc.databuild.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bonc.databuild.entity.BusiCatagory;
import com.bonc.databuild.form.ApiQueryForm;
import com.bonc.databuild.form.BranchResultForm;
import com.bonc.databuild.service.BranchService;
import com.bonc.databuild.service.QuotaInfoService;
import com.bonc.databuild.utils.R;
import com.bonc.databuild.utils.StringUtils;
import jdk.nashorn.internal.objects.annotations.Where;
import org.apache.ibatis.annotations.Case;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class DataProductionController {
    private static  final Logger logger = LoggerFactory.getLogger(DataProductionController.class);

    @Autowired
    private BranchService branchService;
    @Autowired
    private QuotaInfoService quotaInfoService;

    public static final Map<String,String> QUOTA_VALUE_ITEM_MAP = new HashMap<String, String>(){
        {
            put("ITEM_VAL","日指标值");
            put("PCT_YESTERDAY_INCT_VAL","日环比增长值");
            put("PCT_YESTERDAY_INCT_PRCNT","日环比增长率");
            put("PCT_LAST_MON_INCT_VAL","月环比增长值");
            put("PCT_LAST_MON_INCT_PRCNT","月环比增长率");
            put("PCT_LAST_YEAR_INCT_VAL","年同比增长值");
            put("PCT_LAST_YEAR_INCT_PRCNT","年同比增长率");
            put("TMON_VAL","月累计值");
            put("TMON_PCT_LAST_MON_INCT_VAL","月累计环比增长值");
            put("TMON_PCT_LAST_MON_INCT_PRCNT","月累计环比增长率");
            put("TMON_PCT_LAST_YEAR_INCT_VAL","月累计同比增长值");
            put("TMON_PCT_LAST_YEAR_INCT_PRCNT","月累计同比增长率");
            put("TYEAR_VAL","年累计值");
            put("TYEAR_PCT_LAST_YEAR_INCT_VAL","年累计同比增长值");
            put("TYEAR_PCT_LAST_YEAR_INCT_PRCNT","年累计同比增长率");        }
        };
    public static final List<Map<String,Object>> DATA = new ArrayList<Map<String, Object>>(){
        {
            add((Map<String, Object>) new HashMap<>().put("202112160000",167));
            add((Map<String, Object>) new HashMap<>().put("202112160015",1572));
            add((Map<String, Object>) new HashMap<>().put("202112160030",5381));
            add((Map<String, Object>) new HashMap<>().put("202112160045",9963));
            add((Map<String, Object>) new HashMap<>().put("202112160100",5122));
            add((Map<String, Object>) new HashMap<>().put("202112160115",6546));
            add((Map<String, Object>) new HashMap<>().put("202112160130",8748));
            add((Map<String, Object>) new HashMap<>().put("202112160145",3055));
            add((Map<String, Object>) new HashMap<>().put("202112160200",9243));
            add((Map<String, Object>) new HashMap<>().put("202112160215",1713));
            add((Map<String, Object>) new HashMap<>().put("202112160230",4003));
            add((Map<String, Object>) new HashMap<>().put("202112160245",9499));
            add((Map<String, Object>) new HashMap<>().put("202112160300",1043));
            add((Map<String, Object>) new HashMap<>().put("202112160315",8891));
            add((Map<String, Object>) new HashMap<>().put("202112160330",3395));
            add((Map<String, Object>) new HashMap<>().put("202112160345",6053));
            add((Map<String, Object>) new HashMap<>().put("202112160400",8518));
            add((Map<String, Object>) new HashMap<>().put("202112160415",561));
            add((Map<String, Object>) new HashMap<>().put("202112160430",9753));
            add((Map<String, Object>) new HashMap<>().put("202112160445",1024));
            add((Map<String, Object>) new HashMap<>().put("202112160500",8392));
            add((Map<String, Object>) new HashMap<>().put("202112160515",2484));
            add((Map<String, Object>) new HashMap<>().put("202112160530",9018));
            add((Map<String, Object>) new HashMap<>().put("202112160545",3366));
            add((Map<String, Object>) new HashMap<>().put("202112160600",4491));
            add((Map<String, Object>) new HashMap<>().put("202112160615",461));
            add((Map<String, Object>) new HashMap<>().put("202112160630",404));
            add((Map<String, Object>) new HashMap<>().put("202112160645",2792));
            add((Map<String, Object>) new HashMap<>().put("202112160700",5501));
            add((Map<String, Object>) new HashMap<>().put("202112160715",8026));
            add((Map<String, Object>) new HashMap<>().put("202112160730",8960));
            add((Map<String, Object>) new HashMap<>().put("202112160745",5182));
            add((Map<String, Object>) new HashMap<>().put("202112160800",6831));
            add((Map<String, Object>) new HashMap<>().put("202112160815",7493));
            add((Map<String, Object>) new HashMap<>().put("202112160830",4020));
            add((Map<String, Object>) new HashMap<>().put("202112160845",4844));
            add((Map<String, Object>) new HashMap<>().put("202112160900",3888));
            add((Map<String, Object>) new HashMap<>().put("202112160915",1650));
            add((Map<String, Object>) new HashMap<>().put("202112160930",5141));
            add((Map<String, Object>) new HashMap<>().put("202112160945",6948));
            add((Map<String, Object>) new HashMap<>().put("202112161000",5847));
            add((Map<String, Object>) new HashMap<>().put("202112161015",4180));
            add((Map<String, Object>) new HashMap<>().put("202112161030",7593));
            add((Map<String, Object>) new HashMap<>().put("202112161045",7506));
            add((Map<String, Object>) new HashMap<>().put("202112161100",9600));
            add((Map<String, Object>) new HashMap<>().put("202112161115",3453));
            add((Map<String, Object>) new HashMap<>().put("202112161130",8842));
            add((Map<String, Object>) new HashMap<>().put("202112161145",649));
            add((Map<String, Object>) new HashMap<>().put("202112161200",9453));
            add((Map<String, Object>) new HashMap<>().put("202112161215",625));
            add((Map<String, Object>) new HashMap<>().put("202112161230",7191));
            add((Map<String, Object>) new HashMap<>().put("202112161245",1319));
            add((Map<String, Object>) new HashMap<>().put("202112161300",3630));
            add((Map<String, Object>) new HashMap<>().put("202112161315",6214));
            add((Map<String, Object>) new HashMap<>().put("202112161330",2229));
            add((Map<String, Object>) new HashMap<>().put("202112161345",6183));
            add((Map<String, Object>) new HashMap<>().put("202112161400",8348));
            add((Map<String, Object>) new HashMap<>().put("202112161415",7002));
            add((Map<String, Object>) new HashMap<>().put("202112161430",1922));
            add((Map<String, Object>) new HashMap<>().put("202112161445",6003));
            add((Map<String, Object>) new HashMap<>().put("202112161500",8094));
            add((Map<String, Object>) new HashMap<>().put("202112161515",8368));
            add((Map<String, Object>) new HashMap<>().put("202112161530",2106));
            add((Map<String, Object>) new HashMap<>().put("202112161545",3747));
            add((Map<String, Object>) new HashMap<>().put("202112161600",2255));
            add((Map<String, Object>) new HashMap<>().put("202112161615",9096));
            add((Map<String, Object>) new HashMap<>().put("202112161630",8434));
            add((Map<String, Object>) new HashMap<>().put("202112161645",4870));
            add((Map<String, Object>) new HashMap<>().put("202112161700",4615));
            add((Map<String, Object>) new HashMap<>().put("202112161715",5162));
            add((Map<String, Object>) new HashMap<>().put("202112161730",7458));
            add((Map<String, Object>) new HashMap<>().put("202112161745",844));
            add((Map<String, Object>) new HashMap<>().put("202112161800",9957));
            add((Map<String, Object>) new HashMap<>().put("202112161815",6831));
            add((Map<String, Object>) new HashMap<>().put("202112161830",6192));
            add((Map<String, Object>) new HashMap<>().put("202112161845",3631));
            add((Map<String, Object>) new HashMap<>().put("202112161900",8366));
            add((Map<String, Object>) new HashMap<>().put("202112161915",7004));
            add((Map<String, Object>) new HashMap<>().put("202112161930",5639));
            add((Map<String, Object>) new HashMap<>().put("202112161945",8796));
            add((Map<String, Object>) new HashMap<>().put("202112162000",3151));
            add((Map<String, Object>) new HashMap<>().put("202112162015",7724));
            add((Map<String, Object>) new HashMap<>().put("202112162030",9687));
            add((Map<String, Object>) new HashMap<>().put("202112162045",8417));
            add((Map<String, Object>) new HashMap<>().put("202112162100",7343));
            add((Map<String, Object>) new HashMap<>().put("202112162115",1721));
            add((Map<String, Object>) new HashMap<>().put("202112162130",7717));
            add((Map<String, Object>) new HashMap<>().put("202112162145",1524));
            add((Map<String, Object>) new HashMap<>().put("202112162200",3872));
            add((Map<String, Object>) new HashMap<>().put("202112162215",2359));
            add((Map<String, Object>) new HashMap<>().put("202112162230",5939));
            add((Map<String, Object>) new HashMap<>().put("202112162245",1805));
            add((Map<String, Object>) new HashMap<>().put("202112162300",3332));
            add((Map<String, Object>) new HashMap<>().put("202112162315",2109));
            add((Map<String, Object>) new HashMap<>().put("202112162330",7627));
            add((Map<String, Object>) new HashMap<>().put("202112162345",7073));

        }
    };

    //localhost:8088/hello
    @PostMapping("/hello")
    public R helloWord(@RequestBody Map<String,Object> params){
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            allParamList = getAllParamList_base(params);
            logger.info("结果：" + allParamList);
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
        return R.ok().put("data",allParamList);
    }
    /**
     * @Description  获取15条指标值数据
     * @param params
     * @return com.conb.dp.utils.R
     * @author yangbo
     * @Date 2021/10/14 16:30
     */
    //http://localhost:8088/getQuotaValue?statisticalDate=2021-10-14&quotaCycle=&region=&city=&gridding=
    @RequestMapping("/getQuotaValue")
    public String getQuotaValue(@RequestBody Map<String,Object> params) throws Exception {
        logger.info("入参参数 = " + params);
        List<Map<String,Object>> list = new ArrayList<>();
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            //参数验证
            checkParams(params);
            logger.info("参数验证完成");
            if (StringUtils.isEmpty(params.get("deal_date"))){
                throw new Exception("deal_date不能为空");
            }

            allParamList = getAllParamList_base(params);
            logger.info("全部列表={}",allParamList);
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }

        for(Map<String,Object>  param : allParamList) {
            param.put("deal_date",params.get("deal_date"));
            param.put("item_code",params.get("item_code"));
            Map<String, Object> result = randomDataBasic(param);
            list.add(result);
        }
       return JSONObject.toJSONString(R.ok().put("data",list));

    }

    @RequestMapping("/getBasicQuotaValue")
    public String getBasicQuotaValue(@RequestBody Map<String,Object> params) throws Exception {
        System.out.println("params =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + params);
        List<Map<String,Object>> list = new ArrayList<>();
        String itemCode = params.get("item_code").toString();
        for(String quotaCode : itemCode.split(",")){
            if ("SY0080D1".equals(quotaCode)){
                List<Map<String,Object>> list1 = new ArrayList<>();
                list.addAll(list1);
            }else {
                System.out.println("quotaCode = >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + quotaCode);
                Map<String, Object> map = new HashMap<>();
                map.put("region_code", params.get("region_code").toString());
                map.put("item_code", quotaCode);
                map.put("city_code", params.get("city_code").toString());
                map.put("gridding_code", params.get("gridding_code").toString());
                map.put("deal_date", params.get("deal_date").toString());
                String quotaValue = this.getQuotaValue(map);
                System.out.println("quotaValue = " + quotaValue);
                JSONObject jsonObject = JSON.parseObject(quotaValue);
                list.addAll((List<Map<String, Object>>) jsonObject.get("data"));
            }
        }
        //Thread.sleep(2000);
        System.out.println("list = " + list);
        return JSONObject.toJSONString(R.ok().put("data",list));
/*
       String result = "{\n" +
               "    \"msg\": \"success\",\n" +
               "    \"code\": 0,\n" +
               "    \"data\": [\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": 7.5279,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": 56219422.41,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0209A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 72535204.08,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": 293892249.26,\n" +
               "            \"TMON_VAL\": 293892249.26,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 32.7684,\n" +
               "            \"TYEAR_VAL\": 803032018.05,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        },\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": 7.7045,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": 57305415.3,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0210A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 66700903.15,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": 293487336.15,\n" +
               "            \"TMON_VAL\": 293487336.15,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 29.4113,\n" +
               "            \"TYEAR_VAL\": 801098421.87,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        },\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": 574387434.95,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0211A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 6566014.22,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": 198756261.9,\n" +
               "            \"TMON_VAL\": 198756261.9,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 3.4164,\n" +
               "            \"TYEAR_VAL\": 574387434.95,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        },\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": 776090056.4,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0212A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 19154122.54,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": 284727684.46,\n" +
               "            \"TMON_VAL\": 284727684.46,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 7.2124,\n" +
               "            \"TYEAR_VAL\": 776090056.4,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        },\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": -1.0556,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": -4680233.83,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": -7.9753,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0213A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 13038965.07,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": -13194057.84,\n" +
               "            \"TMON_VAL\": 152242418.55,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 9.3668,\n" +
               "            \"TYEAR_VAL\": 438675558.23,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        },\n" +
               "        {\n" +
               "            \"CITY_DESC\": \"汇总\",\n" +
               "            \"REGION_DESC\": \"太原地区\",\n" +
               "            \"CITY_CODE\": \"00\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_PRCNT\": 24.9175,\n" +
               "            \"REGION_CODE\": \"01\",\n" +
               "            \"TYEAR_PCT_LAST_YEAR_INCT_VAL\": 48661860.77,\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_PRCNT\": 0,\n" +
               "            \"GRIDDING_CODE\": \"00\",\n" +
               "            \"ITEM_CODE\": \"SRCW0214A\",\n" +
               "            \"TMON_PCT_LAST_MON_INCT_VAL\": 42246558.41,\n" +
               "            \"GRIDDING_DESC\": \"汇总\",\n" +
               "            \"TMON_PCT_LAST_YEAR_INCT_VAL\": 102751729.6,\n" +
               "            \"TMON_VAL\": 102751729.6,\n" +
               "            \"TMON_PCT_LAST_MON_INCT_PRCNT\": 69.8231,\n" +
               "            \"TYEAR_VAL\": 243954076.22,\n" +
               "            \"DEAL_DATE\": \"202203\"\n" +
               "        }\n" +
               "    ]\n" +
               "}";
       return result;
*/

    }
    @RequestMapping("/getTendencyDay")
    public String getTendencyDay(@RequestBody Map<String,Object> params) throws Exception {
        Long startTime = System.currentTimeMillis();
        System.out.println("params =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + params);
        List<Map<String,Object>> list = new ArrayList<>();
        String itemCode = params.get("item_code").toString();
        for(String quotaCode : itemCode.split(",")){
            Map<String,Object> map = new HashMap<>();
            map.put("region_code",params.get("region_code"));
            map.put("item_code",quotaCode);
            map.put("city_code",params.get("city_code"));
            map.put("gridding_code",params.get("gridding_code"));
            map.put("deal_date",params.get("deal_date"));
            String quotaValue = this.getTendencyValue_day(map);
            JSONObject jsonObject = JSON.parseObject(quotaValue);
            System.out.println("jsonObject.get(\"data\") = " + jsonObject.get("data"));
            list.addAll((List<Map<String, Object>>) jsonObject.get("data"));
        }
       Long endTime = System.currentTimeMillis();
        logger.info("数据生成用时={}ms",(endTime - startTime));
        return JSONObject.toJSONString(R.ok().put("data",list));
       /* Long startTime = System.currentTimeMillis();
        String result = "{\n" +
                "    \"msg\": \"success\",\n" +
                "    \"code\": 0,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2785205.73,\n" +
                "            \"ITEM_CODE\": \"ZW0119D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2828107.32,\n" +
                "            \"ITEM_CODE\": \"ZW0117D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 1833678.96,\n" +
                "            \"ITEM_CODE\": \"ZW0116D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2191.05,\n" +
                "            \"ITEM_CODE\": \"ZW0120D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 20186.71,\n" +
                "            \"ITEM_CODE\": \"ZW0121D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2828107.32,\n" +
                "            \"ITEM_CODE\": \"ZW0117D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2191.05,\n" +
                "            \"ITEM_CODE\": \"ZW0120D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 1833678.96,\n" +
                "            \"ITEM_CODE\": \"ZW0116D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 20186.71,\n" +
                "            \"ITEM_CODE\": \"ZW0121D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 2785205.73,\n" +
                "            \"ITEM_CODE\": \"ZW0119D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 4929312.65,\n" +
                "            \"ITEM_CODE\": \"ZW0115D1\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 4929312.65,\n" +
                "            \"ITEM_CODE\": \"ZW0115D3\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-04-18\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        Long endTime = System.currentTimeMillis();
        logger.info("数据生成用时={}ms",(endTime - startTime));
        return result;*/
    }

    @RequestMapping("/getTendencyMonth")
    public String getTendencyMonth(@RequestBody Map<String,Object> params) throws Exception {
        Long startTime = System.currentTimeMillis();
        System.out.println("params =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + params);
        List<Map<String,Object>> list = new ArrayList<>();
        String itemCode = params.get("item_code").toString();
        for(String quotaCode : itemCode.split(",")){
            Map<String,Object> map = new HashMap<>();
            map.put("region_code",params.get("region_code"));
            map.put("item_code",quotaCode);
            map.put("city_code",params.get("city_code"));
            map.put("gridding_code",params.get("gridding_code"));
            map.put("deal_date",params.get("deal_date"));
            String quotaValue = this.getTendencyValue_month(map);
            JSONObject jsonObject = JSON.parseObject(quotaValue);
            list.addAll((List<Map<String, Object>>) jsonObject.get("data"));
        }
        Long endTime = System.currentTimeMillis();
        logger.info("数据生成用时={}ms",(endTime - startTime));
        return JSONObject.toJSONString(R.ok().put("data",list));

       /* String result = "{\n" +
                "    \"msg\": \"success\",\n" +
                "    \"code\": 0,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 102751729.6,\n" +
                "            \"ITEM_CODE\": \"SRCW0214A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 152242418.55,\n" +
                "            \"ITEM_CODE\": \"SRCW0213A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 284727684.46,\n" +
                "            \"ITEM_CODE\": \"SRCW0212A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 198756261.9,\n" +
                "            \"ITEM_CODE\": \"SRCW0211A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 293487336.15,\n" +
                "            \"ITEM_CODE\": \"SRCW0210A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 293892249.26,\n" +
                "            \"ITEM_CODE\": \"SRCW0209A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 60505171.19,\n" +
                "            \"ITEM_CODE\": \"SRCW0214A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 139203453.48,\n" +
                "            \"ITEM_CODE\": \"SRCW0213A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 265573561.92,\n" +
                "            \"ITEM_CODE\": \"SRCW0212A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 192190247.68,\n" +
                "            \"ITEM_CODE\": \"SRCW0211A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 226786433,\n" +
                "            \"ITEM_CODE\": \"SRCW0210A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 221357045.18,\n" +
                "            \"ITEM_CODE\": \"SRCW0209A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202202\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 58309078.63,\n" +
                "            \"ITEM_CODE\": \"SRCW0214A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 146424228.57,\n" +
                "            \"ITEM_CODE\": \"SRCW0213A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 225788810.02,\n" +
                "            \"ITEM_CODE\": \"SRCW0212A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 183440925.37,\n" +
                "            \"ITEM_CODE\": \"SRCW0211A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 233899371.29,\n" +
                "            \"ITEM_CODE\": \"SRCW0210A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 235333205.13,\n" +
                "            \"ITEM_CODE\": \"SRCW0209A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"202201\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        return result;
*/
    }



    private Map<String, Object> randomDataBasic(Map<String, Object> params) {
        Map<String,Object> result = new HashMap<String, Object>();
        result.put("ITEM_CODE",params.get("item_code"));
        result.put("REGION_CODE",params.get("region_code"));
        result.put("REGION_DESC",params.get("region_desc"));
        result.put("CITY_CODE",params.get("city_code"));
        result.put("CITY_DESC",params.get("city_desc"));
        result.put("GRIDDING_CODE",params.get("gridding_code"));
        result.put("GRIDDING_DESC",params.get("gridding_desc"));
        result.put("DEAL_DATE",params.get("deal_date").toString());
        Random random = new Random();
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        for(String str:QUOTA_VALUE_ITEM_MAP.keySet()){
            if (QUOTA_VALUE_ITEM_MAP.get(str).contains("值")){
                    result.put(str, random.nextInt(12345) + 1234);
            }
            if (QUOTA_VALUE_ITEM_MAP.get(str).contains("率")){
                    result.put(str, decimalFormat.format(0.5 - (random.nextDouble() + 0.0123)));
            }
        }
        logger.info("结果：" + result);
        return result;
    }

    /**
     * 参数验证
     */
    private void checkParams(Map<String, Object> params) throws Exception {
        if (StringUtils.isEmpty(params.get("item_code"))){
            throw new Exception("item_code不能为空");
        }
        if (StringUtils.isEmpty(params.get("region_code"))){
            throw new Exception("region_code不能为空");
        }
        if (StringUtils.isEmpty(params.get("city_code"))){
            throw new Exception("city_code不能为空");
        }
        if (StringUtils.isEmpty(params.get("gridding_code"))){
            throw new Exception("gridding_code不能为空");
        }

    }

    /**
     * @Description 日趋势图
     * @param params
     * @return com.conb.dp.utils.R
     * @author yangbo
     * @Date 2021/10/14 17:28
     */
    //http://localhost:8088/getTendencyValue?statisticalDate=2021-10-14&quotaCycle=00&region=&city=&gridding=
    @RequestMapping("/getTendencyValue_day")
    public String getTendencyValue_day(@RequestBody Map<String,Object> params) throws Exception {
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            //参数验证
            checkParams(params);
            if (StringUtils.isEmpty(params.get("deal_date"))){
                throw new Exception("deal_date不能为空");
            }
            allParamList = getAllParamList_base(params);
            BeanUtils.copyProperties(allParamList.get(0),params);
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }
        //获取查询时间
        Random random = new Random();
        String statisticalDate = String.valueOf(params.get("deal_date"));
        logger.info("statisticalDate = " + statisticalDate);
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Date date =sdf.parse(statisticalDate);


        List<Map<String,Object>> list = new ArrayList<>();

        for(int i = 0; i <30;i++){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("ITEM_CODE",params.get("item_code"));
            map.put("REGION_CODE",params.get("region_code"));
            map.put("CITY_CODE",params.get("city_code"));
            map.put("GRIDDING_CODE",params.get("gridding_code"));
            calendar.add(Calendar.DAY_OF_MONTH,0-i);
            map.put("DEAL_DATE",sdf.format(calendar.getTime()));
            map.put("ITEM_VALUE",random.nextInt(12345)+1234);

            list.add(map);
        }
        logger.info("日趋势图查询结果：" + list);
        return JSONObject.toJSONString(R.ok().put("data",list));
     /*   String result = "{\n" +
                "    \"msg\": \"success\",\n" +
                "    \"code\": 0,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 754204,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-28\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 752149,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-27\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 751299,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-26\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 750015,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-25\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 748291,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-24\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 736810,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-17\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 144650,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-02-28\"\n" +
                "        }\n" +
                "    ]\n" +
                "}\n";
        return result;*/
    }
    /**
     * @Description 月趋势图
     * @param params
     * @return java.lang.String
     * @author yangbo
     * @Date 2022/4/12 9:44
     */
    @RequestMapping("/getTendencyValue_month")
    public String getTendencyValue_month(@RequestBody Map<String,Object> params) throws Exception {
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            //参数验证
            checkParams(params);
            if (StringUtils.isEmpty(params.get("deal_date"))){
                throw new Exception("deal_date不能为空");
            }
            allParamList = getAllParamList_base(params);
            BeanUtils.copyProperties(allParamList.get(0),params);
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }
        //获取查询时间
        Random random = new Random();
        String statisticalDate = String.valueOf(params.get("deal_date"));
        logger.info("statisticalDate = " + statisticalDate);
        SimpleDateFormat sdf ;
        if (statisticalDate.split("-").length==3) {

           sdf = new SimpleDateFormat("yyyy-MM-dd");
        }else {
           sdf = new SimpleDateFormat("yyyy-MM");
        }
        Date date =sdf.parse(statisticalDate);


        List<Map<String,Object>> list = new ArrayList<>();

        for(int i = 0; i <12;i++){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("ITEM_CODE",params.get("item_code"));
            map.put("REGION_CODE",params.get("region_code"));
            map.put("CITY_CODE",params.get("city_code"));
            map.put("GRIDDING_CODE",params.get("gridding_code"));
            calendar.add(Calendar.MONTH,0-i);
            map.put("DEAL_DATE",sdf.format(calendar.getTime()));
            map.put("ITEM_VALUE",random.nextInt(12345)+1234);

            list.add(map);
        }
        logger.info("月趋势图查询结果：" + list);
        return JSONObject.toJSONString(R.ok().put("data",list));
        /*String result = "{\n" +
                "    \"msg\": \"success\",\n" +
                "    \"code\": 0,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 754204,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-03-28\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"CITY_CODE\": \"00\",\n" +
                "            \"REGION_CODE\": \"01\",\n" +
                "            \"ITEM_VALUE\": 144650,\n" +
                "            \"ITEM_CODE\": \"XXMG0165A\",\n" +
                "            \"GRIDDING_CODE\": \"00\",\n" +
                "            \"DEAL_DATE\": \"2022-02-28\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        return result;*/
    }

    /**
     * @Description 获取区域图
     * @param params   region--地市码   city--区县码  gridding--网格码  statisticalDate--统计日期   quotaCycle--统计周期 quotaType---指标粒度
     * @return com.conb.dp.utils.R
     * @author yangbo
     * @Date 2021/10/15 11:01
     */
//    http://localhost:8088/getAreaValue?region=01&city=02&gridding=&statisticalDate=2021-10-11&quotaCycle=03&quotaType=3
    @RequestMapping("/getAreaValue")
    public String getAreaValue(@RequestBody Map<String,Object> params) throws Exception {
        logger.info("入参参数：" + params);
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            //参数验证
            checkParams(params);
            if (StringUtils.isEmpty(params.get("deal_date"))){
                throw new Exception("deal_date不能为空");
            }
            allParamList = getAllParamList_base(params);
            BeanUtils.copyProperties(allParamList.get(0),params);
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }
        Random random = new Random();
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        Map<String,Object> resultMap = new HashMap<String, Object>();
        List<BranchResultForm> branchResultFormList = new ArrayList<>();
        List<String> xData = new ArrayList<String>();
        List<Integer> yData = new ArrayList<Integer>();

        String quotaType = quotaInfoService.getQuotaType(String.valueOf(params.get("item_code")));//网格  行政
        String region= String.valueOf(params.get("region_code"));
        String city= "00".equals(String.valueOf(params.get("city_code")))?"":String.valueOf(params.get("city_code"));
        String gridding= "00".equals(String.valueOf(params.get("gridding_code")))?"":String.valueOf(params.get("gridding_code"));
        int flag = 0;
        if (region!=null && !"".equals(region)){
            flag++;
        }
        if (city!=null && !"".equals(city)){
            flag++;
        }
        if (gridding!=null && !"".equals(gridding)){
            flag++;
        }
      logger.info("flag = " + flag);

            if (flag==1){
                if ("00".equals(region)){//全省
                    branchResultFormList =branchService.queryRegionList(region);
                    //xData = branchService.getRegionList(1);
                }else {
                    branchResultFormList = branchService.queryCityList_(region,city);
                    //xData = branchService.getCityList(region);
                }

            }else if (flag==2){
                if ("1".equals(quotaType)){
                    branchResultFormList = branchService.queryCityListNormal(region,city);
                    //xData = branchService.getCity(region,city);
                }else if ("3".equals(quotaType)){
                    branchResultFormList = branchService.queryGriddingList_(region+city,null);
                    //xData = branchService.getGriddingList(region,city,null);
                }else {
                    branchResultFormList = null;
                    //xData=null;
                }
            }else if (flag==3){
                if ("1".equals(quotaType)) {//行政
                    branchResultFormList = null;
                    //xData=null;
                }else if ("3".equals(quotaType)) {//wangge
                    branchResultFormList = branchService.queryGriddingListNormal(region+city,gridding);
                    //xData = branchService.getGriddingList(region, city, gridding);
                }else {
                    //xData=null;
                    branchResultFormList = null;
                }

            }
            List<Map<String,Object>> list = new ArrayList<>();
            if (branchResultFormList!=null && branchResultFormList.size()!=0){
                for(BranchResultForm branchResultForm  : branchResultFormList){
                    xData.add(branchResultForm.getName());
                    yData.add(random.nextInt(12345) + 1234);
                    Map<String, Object> map = new HashMap<>();
                    if (region.equals("00")){
                        map.put("REGION_CODE",branchResultForm.getCode());
                        map.put("CITY_CODE","00");
                        map.put("GRIDDING_CODE","00");
                    }else {
                        map.put("REGION_CODE",region);
                        if (StringUtils.isEmpty(city)){
                            map.put("CITY_CODE",branchResultForm.getCode());
                            map.put("GRIDDING_CODE","00");
                        }else {
                            map.put("CITY_CODE",city);
                            map.put("GRIDDING_CODE",branchResultForm.getCode());
                        }
                    }


                    map.put("VALUE_NAME",branchResultForm.getName());
                    boolean randomFlag = random.nextInt(10)>=5;
                    for (String valueItem : QUOTA_VALUE_ITEM_MAP.keySet()) {
                        if (QUOTA_VALUE_ITEM_MAP.get(valueItem).contains("值")) {
                            if (randomFlag) {
                                map.put(valueItem, random.nextInt(123452) + 1234);
                            }else {
                                map.put(valueItem, random.nextInt(12345) + 1234);
                            }
                        } else if (QUOTA_VALUE_ITEM_MAP.get(valueItem).contains("率")) {
                            map.put(valueItem, decimalFormat.format(0.5-(random.nextDouble() + 0.0123)));
                        }
                    }
                    list.add(map);
                }

            }
        //resultMap.put("list",list);
        //resultMap.put("xData",xData);
        //resultMap.put("yData",yData);
        logger.info("结果：" + list);
        return JSONObject.toJSONString(R.ok().put("data",list));
    }
    /**
     * @Description 实时指标值列表
     * @param params
     * @return java.lang.String
     * @author yangbo
     * @Date 2022/1/5 17:53
     */
    @RequestMapping("/rTimeValue")
    public String rTimeVlue(@RequestBody Map<String,Object> params) throws Exception {
        logger.info("入参参数：" + params);
        List<Map<String,Object>> list = new ArrayList<>();
        //根据参数不同获取需要生成数据的列表
        try {
            //参数验证
            checkParams(params);
            List<Map<String,Object>> paramList = getAllParamList_base(params);
            if (paramList.size()!=0) {
                for (Map<String, Object> param : paramList) {
                    Map<String, Object> map = randomData(param);
                    list.add(map);
                }
            }
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }
        logger.info("最终结果：" + list);
        //list = new ArrayList<>();
        return JSONObject.toJSONString(R.ok().put("data",list));
        //String result = "{\n" +
        //        "    \"msg\": \"success\",\n" +
        //        "    \"code\": 0,\n" +
        //        "    \"data\": [\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 09:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"LAST_DAY_ITEM_VALUE\": 1887,\n" +
        //        "            \"ITEM_CODE\": \"A000\",\n" +
        //        "            \"CUR_ITEM_VALUE\": 120,\n" +
        //        "            \"LAST_DAY_CUR_ITEM_VALUE\": null\n" +
        //        "        }\n" +
        //        "    ]\n" +
        //        "}";
        //return result;
    }
    /**
     * 实时指标 获取全部的参数情况
     */
    private List<Map<String, Object>> getAllParamList(Map<String, Object> params) throws Exception {
        //所有情况列表
        List<Map<String ,Object>> paramList = new ArrayList<>();
        List<BranchResultForm> regionList = new ArrayList<>();
        if ("00".equals(params.get("region_code"))){
            Map<String,Object> map = new HashMap<>();
            map.put("region_code","00");
            map.put("city_code","00");
            map.put("gridding_code","00");
            paramList.add(map);
        }else if ("_".equals(params.get("region_code"))){
            throw new Exception("错误的sql语句");
        }else {
            String regionCode = params.get("region_code").toString();
            regionList = branchService.queryRegionList(regionCode);
            for(BranchResultForm region : regionList){
                if ("00".equals(params.get("city_code"))||"_".equals(params.get("city_code"))) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("region_code", region.getCode());
                    map.put("city_code", "00");
                    map.put("gridding_code", "00");
                    paramList.add(map);
                }
                List<BranchResultForm> cityList= new ArrayList<>();
                if ("00".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList00(region.getCode(),params.get("city_code").toString());
                }else if ("_".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList_(region.getCode(),params.get("city_code").toString());
                }else {
                    cityList = branchService.queryCityListNormal(region.getCode(),params.get("city_code").toString());
                }
                if (cityList.size()!=0){
                    for(BranchResultForm city:cityList){
                        if ("00".equals(params.get("gridding_code"))||"_".equals(params.get("gridding_code"))) {
                            Map<String, Object> map1 = new HashMap<>();
                            map1.put("region_code", region.getCode());
                            map1.put("city_code", city.getCode());
                            map1.put("gridding_code", "00");
                            paramList.add(map1);
                        }
                        List<BranchResultForm> griddingList = new ArrayList<>();
                        if ("00".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList00(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else if ("_".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList_(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else {
                            griddingList = branchService.queryGriddingListNormal(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }
                        if (griddingList.size()!=0){
                            for(BranchResultForm gridding:griddingList) {
                                Map<String, Object> map2 = new HashMap<>();
                                map2.put("region_code", region.getCode());
                                map2.put("city_code", city.getCode());
                                map2.put("gridding_code", gridding.getCode());
                                paramList.add(map2);
                            }
                        }
                    }
                }
            }
        }
        return paramList;
    }

    private List<Map<String,Object>> getAllParamList_base(Map<String,Object> params){
        logger.info("获取全部列表");
        List<Map<String,Object>> paramList = new ArrayList<>();
        List<BranchResultForm> regionList;
        try {

        if ("00".equals(params.get("region_code"))){
            logger.info("region_code==00");
            Map<String,Object> map = new HashMap<>();
            map.put("region_code","00");
            map.put("region_desc","汇总");
            map.put("city_code","00");
            map.put("city_desc","汇总");
            map.put("gridding_code","00");
            map.put("gridding_desc","汇总");
            paramList.add(map);
        }else if ("_".equals(params.get("region_code"))){
            logger.info("region_code==_");
            Map<String,Object> map = new HashMap<>();
            map.put("region_code","00");
            map.put("region_desc","汇总");
            map.put("city_code","00");
            map.put("city_desc","汇总");
            map.put("gridding_code","00");
            map.put("gridding_desc","汇总");
            paramList.add(map);
            //获取所有地市汇总
            regionList = branchService.queryRegionListNew("_");
            logger.info("regionList={}",regionList);
            for(BranchResultForm branchResultForm:regionList){
                Map<String,Object> map1 = new HashMap<>();
                map1.put("region_code",branchResultForm.getCode());
                map1.put("region_desc",branchResultForm.getName());
                map1.put("city_code","00");
                map1.put("city_desc","汇总");
                map1.put("gridding_code","00");
                map1.put("gridding_desc","汇总");
                paramList.add(map1);
                List<BranchResultForm> cityList= new ArrayList<>();
                if ("00".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList00(branchResultForm.getCode(),params.get("city_code").toString());
                }else if ("_".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList_(branchResultForm.getCode(),params.get("city_code").toString());
                }else {
                    cityList = branchService.queryCityListNormal(branchResultForm.getCode(),params.get("city_code").toString());
                }
                if (cityList.size()!=0){
                    for(BranchResultForm city:cityList){
                        if ("00".equals(params.get("gridding_code"))||"_".equals(params.get("gridding_code"))) {
                            Map<String, Object> map2 = new HashMap<>();
                            map2.put("region_code", branchResultForm.getCode());
                            map2.put("region_desc",branchResultForm.getName());
                            map2.put("city_code", city.getCode());
                            map2.put("city_desc",city.getName());
                            map2.put("gridding_code", "00");
                            map2.put("gridding_desc","汇总");
                            paramList.add(map2);
                        }
                        List<BranchResultForm> griddingList = new ArrayList<>();
                        if ("00".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList00(branchResultForm.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else if ("_".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList_(branchResultForm.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else {
                            griddingList = branchService.queryGriddingListNormal(branchResultForm.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }
                        if (griddingList.size()!=0){
                            for(BranchResultForm gridding:griddingList) {
                                Map<String, Object> map2 = new HashMap<>();
                                map2.put("region_code", branchResultForm.getCode());
                                map2.put("region_desc",branchResultForm.getName());
                                map2.put("city_code", city.getCode());
                                map2.put("city_desc",city.getName());
                                map2.put("gridding_code", gridding.getCode());
                                map2.put("gridding_desc",gridding.getName());
                                paramList.add(map2);
                            }
                        }
                    }
                }


            }
        }else {
            logger.info("region_code==" + params.get("region_code").toString());
            String regionCode = params.get("region_code").toString();
            regionList = branchService.queryRegionListNew(regionCode);
            for(BranchResultForm region : regionList){
                if ("00".equals(params.get("city_code"))||"_".equals(params.get("city_code"))) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("region_code", region.getCode());
                    map.put("region_desc",region.getName());
                    map.put("city_code", "00");
                    map.put("city_desc","汇总");
                    map.put("gridding_code", "00");
                    map.put("gridding_desc","汇总");
                    paramList.add(map);
                }
                List<BranchResultForm> cityList= new ArrayList<>();
                if ("00".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList00(region.getCode(),params.get("city_code").toString());
                }else if ("_".equals(params.get("city_code"))){
                    cityList = branchService.queryCityList_(region.getCode(),params.get("city_code").toString());
                }else {
                    cityList = branchService.queryCityListNormal(region.getCode(),params.get("city_code").toString());
                }
                if (cityList.size()!=0){
                    for(BranchResultForm city:cityList){
                        if ("00".equals(params.get("gridding_code"))||"_".equals(params.get("gridding_code"))) {
                            Map<String, Object> map1 = new HashMap<>();
                            map1.put("region_code", region.getCode());
                            map1.put("region_desc",region.getName());
                            map1.put("city_code", city.getCode());
                            map1.put("city_desc",city.getName());
                            map1.put("gridding_code", "00");
                            map1.put("gridding_desc","汇总");
                            paramList.add(map1);
                        }
                        List<BranchResultForm> griddingList = new ArrayList<>();
                        if ("00".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList00(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else if ("_".equals(params.get("gridding_code"))){
                            griddingList = branchService.queryGriddingList_(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }else {
                            griddingList = branchService.queryGriddingListNormal(region.getCode()+city.getCode(),params.get("gridding_code").toString());
                        }
                        if (griddingList.size()!=0){
                            for(BranchResultForm gridding:griddingList) {
                                Map<String, Object> map2 = new HashMap<>();
                                map2.put("region_code", region.getCode());
                                map2.put("region_desc",region.getName());
                                map2.put("city_code", city.getCode());
                                map2.put("city_desc",city.getName());
                                map2.put("gridding_code", gridding.getCode());
                                map2.put("gridding_desc",gridding.getName());
                                paramList.add(map2);
                            }
                        }
                    }
                }
            }
        }

        }catch (Exception e){
            logger.info("异常={}",e);
        }
        System.out.println("paramList = " + paramList);
        return paramList;
    }
    /**
     * 实时指标 生成随机数据
     */
    private Map<String,Object> randomData(Map<String,Object> params){
        logger.info("随机入参：" + params);
        Map<String,Object> result = new HashMap<String, Object>();
        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.SECOND,0);
        if (calendar.get(Calendar.MINUTE)>=0&& calendar.get(Calendar.MINUTE)<15){
            calendar.set(Calendar.MINUTE,0);
        }else if (calendar.get(Calendar.MINUTE)>=15&& calendar.get(Calendar.MINUTE)<30){
            calendar.set(Calendar.MINUTE,15);
        }else if (calendar.get(Calendar.MINUTE)>=30&& calendar.get(Calendar.MINUTE)<45){
            calendar.set(Calendar.MINUTE,30);
        }else if (calendar.get(Calendar.MINUTE)>=45&& calendar.get(Calendar.MINUTE)<60){
            calendar.set(Calendar.MINUTE,45);
        }
        result.put("ITEM_CODE",params.get("item_code"));
        result.put("REGION_CODE",params.get("region_code"));
        result.put("CITY_CODE",params.get("city_code"));
        result.put("GRIDDING_CODE",params.get("gridding_code"));
        result.put("DEAL_TIME",sdf.format(calendar.getTime()));
        result.put("CUR_ITEM_VALUE",random.nextInt(12345)+1234);
        result.put("LAST_DAY_CUR_ITEM_VALUE",random.nextInt(12345)+1234);
        result.put("LAST_DAY_ITEM_VALUE",random.nextInt(54321)+1234);
        logger.info("结果：" + result);
        return result;
    }
    /**
     * @Description 实时指标曲线图
     * @param params
     * @return java.lang.String
     * @author yangbo
     * @Date 2022/1/5 17:53
     */
    @RequestMapping("/rTimeTendencyValue")
    public String rTimeTendencyValue(@RequestBody Map<String,Object> params) throws Exception {
        logger.info("入参参数：" + params);
        List<Map<String, Object>> allParamList = new ArrayList<>();
        try {
            //参数验证
            checkParams(params);
            allParamList = getAllParamList_base(params);
            BeanUtils.copyProperties(allParamList.get(0),params);
        }catch (Exception e){
            return JSONObject.toJSONString(R.error(e.getMessage()));
        }
        List<Map<String,Object>> list = new ArrayList<>();

        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar endCalendar = Calendar.getInstance();
        Date date = new Date();
        endCalendar.setTime(date);
        endCalendar.set(Calendar.SECOND,0);
        if (endCalendar.get(Calendar.MINUTE)>=0&& endCalendar.get(Calendar.MINUTE)<15){
            endCalendar.set(Calendar.MINUTE,0);
        }else if (endCalendar.get(Calendar.MINUTE)>=15&& endCalendar.get(Calendar.MINUTE)<30){
            endCalendar.set(Calendar.MINUTE,15);
        }else if (endCalendar.get(Calendar.MINUTE)>=30&& endCalendar.get(Calendar.MINUTE)<45){
            endCalendar.set(Calendar.MINUTE,30);
        }else if (endCalendar.get(Calendar.MINUTE)>=45&& endCalendar.get(Calendar.MINUTE)<60){
            endCalendar.set(Calendar.MINUTE,45);
        }

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.set(Calendar.HOUR_OF_DAY,0);
        startCalendar.set(Calendar.MINUTE,0);
        startCalendar.set(Calendar.SECOND,0);
        while (startCalendar.compareTo(endCalendar)<=0){
            Map<String,Object> result = new HashMap<String, Object>();
            result.put("ITEM_CODE",params.get("item_code"));
            result.put("REGION_CODE",params.get("region_code"));
            result.put("CITY_CODE",params.get("city_code"));
            result.put("GRIDDING_CODE",params.get("gridding_code"));
            result.put("DEAL_TIME",sdf.format(startCalendar.getTime()));
            result.put("ITEM_VALUE",random.nextInt(12345)+1234);
            list.add(result);
            startCalendar.add(Calendar.MINUTE,15);
        }
        logger.info("结果：" + list);
        Collections.shuffle(list);
        return JSONObject.toJSONString(R.ok().put("data",list));
        //String result = "{\n" +
        //        "    \"msg\": \"success\",\n" +
        //        "    \"code\": 0,\n" +
        //        "    \"data\": [\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 08:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 24,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 07:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 8,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 05:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 01:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 01:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 05:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 03:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 04:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 04:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 08:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 9,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 05:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 03:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 04:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 04:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 01:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 06:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 06:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 03:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 03:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 08:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 8,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 07:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 6,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 02:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 02:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 23:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1883,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 15:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1100,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 16:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1343,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 20:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1848,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 20:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1867,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 08:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 16,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 06:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 01:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 05:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 18:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1767,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 19:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1783,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 18:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1649,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 22:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1882,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 21:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1880,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 16:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1223,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 23:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1885,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 09:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 40,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 21:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1872,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 18:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1717,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 19:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1837,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 23:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1887,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 19:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1823,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 09:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 94,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 22:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1881,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 22:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1881,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 19:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1809,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 09:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 120,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 17:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1599,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 07:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 5,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 06:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 4,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 02:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 02:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 2,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 20:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1861,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 17:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1411,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 22:45:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1882,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 23:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1882,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 20:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1851,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 16:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1279,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 17:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1460,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 21:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1877,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 16:00:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1161,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 21:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1880,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 17:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1538,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 15:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1059,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-10 09:15:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 63,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        },\n" +
        //        "        {\n" +
        //        "            \"CITY_CODE\": \"00\",\n" +
        //        "            \"DEAL_TIME\": \"2022-06-09 18:30:00\",\n" +
        //        "            \"REGION_CODE\": \"01\",\n" +
        //        "            \"ITEM_VALUE\": 1736,\n" +
        //        "            \"ITEM_CODE\": \"A000\"\n" +
        //        "        }\n" +
        //        "    ]\n" +
        //        "}";
        //return result;
    }
    @GetMapping("/oozieweb/task/getTaskStateById")
    public String eltInfo(String taskId){
        if ("116750".equals(taskId)){
            String result = "[{\n" +
                    "\t\"start_time\": \"2022-02-01T00:20:50.000+0000\",\n" +
                    "\t\"lifecycle_status_name\": \"已上线\",\n" +
                    "\t\"state_desc\": \"成功\",\n" +
                    "\t\"end_time\": \"2022-02-01T00:21:18.000+0000\",\n" +
                    "\t\"expect_time\": \"2022-01-30T16:00:00.000+0000\",\n" +
                    "\t\"lifecycle_status_code\": \"Online\",\n" +
                    "\t\"id\": \"116750\",\n" +
                    "\t\"state_code\": \"succeed\",\n" +
                    "\t\"name_cn\": \"步步为赢-月新增地市用户数\",\n" +
                    "\t\"name_en\": \"TB_RPT_BBWY_REGION_CNT_DAY\"\n" +
                    "}]\n";
            return result;
        }else {
            String result = "[]";
            return result;
        }
    }

    @RequestMapping("/getBusiCatagoryList")
    public String getBusiCatagoryList(HttpServletRequest request){
        String apiSecret = request.getHeader("apiSecret");
        System.out.println("apiSecret = " + apiSecret);
        if (!"8191154c-416a-4a2b-fc48-8e1155084882".equals(apiSecret)){
            return JSONObject.toJSONString(R.error("apiSecret不匹配"));
        }
        List<BusiCatagory> list = quotaInfoService.getBusiCatagoryList();
        System.out.println("list = " + list);
        return JSONObject.toJSONString(R.ok().put("data",list));
    }




}

package com.bonc.databuild.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bonc.databuild.entity.BusiCatagory;
import com.bonc.databuild.entity.QuotaInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 指标基础信息表
 *
 * @author bonc
 * @email bonc@@bonc.com.cn
 * @date 2021-09-26 14:54:34
 */
@Mapper
public interface QuotaInfoDao extends BaseMapper<QuotaInfoEntity> {
    String getCycle(String quotaCode);

    String getQuotaType(String quotaCode);

    List<BusiCatagory> getBusiCatagoryList();
}

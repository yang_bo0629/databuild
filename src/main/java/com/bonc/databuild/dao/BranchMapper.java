package com.bonc.databuild.dao;

import com.bonc.databuild.form.BranchResultForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BranchMapper {
    List<String> getRegionList(@Param("type") int type);

    List<String> getCityList(@Param("region") String region);

    List<String> getGriddingList(@Param("region") String region, @Param("city") String city, @Param("gridding") String gridding);


    List<String> getCity(@Param("region") String region,@Param("city") String city);

    List<BranchResultForm> queryRegionList(String regionCode);
    List<BranchResultForm> queryRegionListNew(String regionCode);
    List<BranchResultForm> queryCityList00(String regionCode, String cityCode);

    List<BranchResultForm> queryCityList_(String regionCode, String cityCode);

    List<BranchResultForm> queryCityListNormal(String regionCode, String cityCode);

    List<BranchResultForm> queryGriddingList00(String parOrgId, String griddingCode);

    List<BranchResultForm> queryGriddingList_(String parOrgId, String griddingCode);

    List<BranchResultForm> queryGriddingListNormal(String parOrgId, String griddingCode);
}

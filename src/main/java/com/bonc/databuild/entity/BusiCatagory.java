package com.bonc.databuild.entity;

import lombok.Data;

/**
 * @author yangbo
 * @Description
 * @Date 2023/1/5$ 10:10$
 */
@Data
public class BusiCatagory {
    private Long cataId;
    private String cataName;
    private String abbr;
    private String cataCode;
    private Integer cataLevel;
    private Long parCataId;
    private Integer cataIndex;
    private String cataNamePath;
    private String cataIdPath;
}

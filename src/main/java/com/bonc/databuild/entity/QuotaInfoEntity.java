package com.bonc.databuild.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 指标基础信息表
 *
 * @author bonc
 * @email bonc@@bonc.com.cn
 * @date 2021-11-24 14:36:41
 */
@Data
@TableName("tb_qc_quota_info")
public class QuotaInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 指标信息主键
	 */
	@TableId
	private Long id;
	/**
	 * 指标名称
	 */
	private String quotaName;
	/**
	 * 指标描述
	 */
	private String quotaDesc;
	/**
	 * 指标分类 原子指标、考核指标、通报指标、实时指标 运营指标 见字典表
	 */
	private String quotaClass;
	/**
	 * 指标类型 见字典表 多个用,分隔 1,2,3
	 */
	private String quotaTypes;
	/**
	 * 指标权属 1 公共指标 2 个人指标  见字典表
	 */
	private String quotaBelong;
	/**
	 * 指标单位 见字典表
	 */
	private String quotaUnit;
	/**
	 * 指标周期  见字典表
	 */
	private String quotaCycle;
	/**
	 * 指标业务口径
	 */
	private String busiCaliber;
	/**
	 * 指标技术口径
	 */
	private String techCaliber;
	/**
	 * 调度作业任务id
	 */
	private String etlTaskId;
	/**
	 * 调度作业来源 见字典表
	 */
	private String etlSource;
	/**
	 * 调度作业名称
	 */
	private String etlJob;
	/**
	 * 指标业务负责人
	 */
	private String busiPerson;
	/**
	 * 指标技术负责人
	 */
	private String techPerson;
	/**
	 * 指标开发人
	 */
	private String developPerson;
	/**
	 * 厂商编码 见字典表
	 */
	private String vendorCode;
	/**
	 * 指标信息展示地址
	 */
	private String quotaUrl;
	/**
	 * 指标创建者
	 */
	private Long quotaCreater;
	/**
	 * 指标创建时间
	 */
	private Date createTime;
	/**
	 * 指标上线时间
	 */
	private String onlineTime;
	/**
	 * 指标下线时间
	 */
	private String offlineTime;
	/**
	 * 指标状态 0：初始创建   1：开发中  2：待审核  3：审核通过  4：已上线  5：已下线  6：审核未通过
	 */
	private Integer quotaStatus;
	/**
	 * 审批内容
	 */
	private String auditContent;
	/**
	 * 备注
	 */
	private String note;

}

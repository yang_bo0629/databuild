package com.bonc.databuild.utils;

/**
 * @author yangbo
 * @Description
 * @Date 2021/12/29$ 14:38$
 */
public class Constants {
    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";
}
